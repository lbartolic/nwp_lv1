<?php
error_reporting(E_ALL);
ini_set('display_errors', 'On');

require __DIR__ . '/vendor/autoload.php';
include_once 'php/DiplomskiRad.php';
use php\DiplomskiRad;

$dr = new DiplomskiRad();
$html = $dr->read();
$created = $dr->create($html);
foreach($created as $rad) {
	$rad->save();
}
?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<h1>Spremljeni podaci</h1>
	<pre><?php print_r($created);?></pre>
</body>
</html>