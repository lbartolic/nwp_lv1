<?php
namespace php;

interface RadInterface {
	public function create($html);
	public function save();
	public function read();
}

?>