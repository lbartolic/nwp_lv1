<?php
namespace php;

include_once 'RadInterface.php';
use php\RadInterface;
use PHPHtmlParser\Dom;

class DiplomskiRad implements RadInterface {
	protected $nazivRada;
	protected $tekstRada;
	protected $linkRada;
	protected $oibTvrtke;

	public function __construct() {}

	public function create($html) {
		$dom = new Dom();
		$dom->load($html);
		$nazivi = $dom->find('.entry-title a');

		$radovi = [];
		for($i = 0; $i < count($nazivi); $i++) {
			$rad = new DiplomskiRad();
			$rad->nazivRada = $dom->find('.entry-title a')[$i]->text;
			$rad->tekstRada = $dom->find('.fusion-post-content-container p')[$i]->text;
			$rad->linkRada = $dom->find('.entry-title a')[$i]->href;
			$img = $dom->find('.attachment-blog-medium')[$i]->src;
			$rad->oibTvrtke = substr($img, strrpos($img, '/') + 1, 11);
			$radovi[$i] = $rad;
		}
		return $radovi;
	}
	public function save() {
		$tekst = $this->tekstRada;
		$naziv = $this->nazivRada;
		$oib = $this->oibTvrtke;
		$link = $this->linkRada;

		$mysqli = new \mysqli("localhost", "root", "password", "nwp_lv1");
		$mysqli->set_charset('utf8');
		$mysqli->query("INSERT INTO diplomski_radovi (naziv, tekst, link, oib) VALUES ('$naziv', '$tekst', '$link', '$oib')");
		$mysqli->close();
	}
	public function read() {
	    $return = file_get_contents('http://stup.etfos.hr/index.php/zavrsni-radovi/2');
	    return $return;
	}
}

?>